<?php

header('Content-type: application/json');

$offset = (isset($_REQUEST['offset'])) ? (int) $_REQUEST['offset'] : 0;
$limit = (isset($_REQUEST['limit'])) ? (int) $_REQUEST['limit'] : 10;

$sortby = (isset($_REQUEST['sortby'])) ? (int) $_REQUEST['sortby'] + 1 : 1;
$asc = ($_REQUEST['asc']) ? 'ASC' : 'DESC';

$dbname = 'test';
$dbuser = 'test';
$dbpass = 'test123';

$cs = "dbname=$dbname user=$dbuser password=$dbpass";
$conn = pg_connect($cs);

// get total row count
$sql = "SELECT COUNT(*) FROM test_table";
$res = pg_query($conn, $sql);
$total = (int) pg_fetch_row($res)[0];

// get current page
$sql = 'SELECT id AS "Fisrt",
               a AS "Second",
               b AS "Third",
               c AS "Fourth"
        FROM test_table';
$sql = "$sql ORDER BY $sortby $asc OFFSET $offset LIMIT $limit";
$res = pg_query($conn, $sql);

$rows = array();
while ($row = pg_fetch_row($res)) {
    array_push($rows, $row);
}

$headers = array();
$n = pg_num_fields($res);
for ($i = 0; $i < $n; $i++) {
    array_push($headers, pg_field_name($res, $i));
}

$data = array();
$data['total'] = (int) $total;
$data['rows'] = $rows;
$data['headers'] = $headers;

echo json_encode($data);

?>
