<?php

header('Content-type: application/json');

$rows = array(
    array('a', 'b', 'c', 'd'),
    array('e', 'f', 'g', 'h'),
    array('i', 'j', 'k'),
    array('l', 'm', 'n'),
    array('o', 'p', 'q', 'r'),
    array('s', 't', 'u', 'v'),
    array('w', 'x', 'y', 'z'),
);

$headers = array("First", "Second");

$data = array();
$data['headers'] = $headers;
$data['rows'] = $rows;

echo json_encode($data);

?>
